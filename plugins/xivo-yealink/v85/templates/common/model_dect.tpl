#!version:1.0.0.1

firmware.url = http://{{ ip }}:{{ http_port }}/firmware/{{ XX_fw_filename }}

over_the_air.url = http://{{ ip }}:{{ http_port }}/firmware/{{ XX_fw_handset_filename }}

over_the_air.url.w56h = http://{{ ip }}:{{ http_port }}/firmware/{{ W56H_fw_handset_filename }}
over_the_air.url.w53h = http://{{ ip }}:{{ http_port }}/firmware/{{ W53H_fw_handset_filename }}
over_the_air.url.w73h = http://{{ ip }}:{{ http_port }}/firmware/{{ W73H_fw_handset_filename }}
over_the_air.url.w59r = http://{{ ip }}:{{ http_port }}/firmware/{{ W59R_fw_handset_filename }}

over_the_air.handset_tip = 0
